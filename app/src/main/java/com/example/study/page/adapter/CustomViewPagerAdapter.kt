package com.example.study.page.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.study.page.fragment.ImageFragment

/**
 * Created by lijinxi on 2024/2/28
 * @Description:
 */
class CustomViewPagerAdapter(
    fragmentActivity: FragmentActivity,
    private val imageResList: List<Int>
) : FragmentStateAdapter(fragmentActivity) {
    override fun getItemCount(): Int {
        return imageResList.size
    }

    override fun createFragment(position: Int): Fragment {
        return ImageFragment.newInstance(imageResList[position])
    }

}