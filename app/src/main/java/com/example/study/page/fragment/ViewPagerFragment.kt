package com.example.study.page.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.viewpager2.widget.ViewPager2
import com.example.study.R
import com.example.study.databinding.FragmentViewPagerBinding
import com.example.study.page.adapter.CustomViewPagerAdapter

class ViewPagerFragment : Fragment() {
    companion object {
        val imageResList = listOf(
            R.drawable.img_1,
            R.drawable.img_2,
            R.drawable.img_3
        )
    }

    private lateinit var binding: FragmentViewPagerBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentViewPagerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewPager.apply {
            adapter = CustomViewPagerAdapter(requireActivity(), imageResList)
            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    Toast.makeText(context, "You slide to $position.", Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

}