package com.example.study.page.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.study.R
import com.example.study.databinding.FragmentTabHostBinding


class TabHostFragment : Fragment() {
    private lateinit var binding: FragmentTabHostBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentTabHostBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tabHost.setup(
            this.requireContext(),
            activity?.supportFragmentManager!!,
            R.id.tab_content_frame
        )
        val tab1 = binding.tabHost.newTabSpec("Tab 1")
        tab1.setIndicator("Tab 1")
        binding.tabHost.addTab(tab1, ImageFragment::class.java, Bundle().apply {
            putInt("image_res", R.drawable.whale)
        })

        val tab2 = binding.tabHost.newTabSpec("Tab 2")
        tab2.setIndicator("Tab 2")
        binding.tabHost.addTab(tab2, ImageFragment::class.java, Bundle().apply {
            putInt("image_res", R.drawable.ic_launcher_background)
        })
        binding.tabHost.setCurrentTabByTag("Tab 2")
    }

}