package com.example.study.page.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.RecyclerView
import com.example.study.databinding.FragmentImageBinding

/**
 * Created by lijinxi on 2024/2/28
 * @Description:
 */
class CustomViewPagerAdapter2(private val imageResList: List<Int>) :
    RecyclerView.Adapter<CustomViewPagerAdapter2.CustomViewHolder>() {

    class CustomViewHolder(private val binding: FragmentImageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(@DrawableRes imgRes: Int) {
            binding.imgView.setImageResource(imgRes)
            binding.textView.text = imgRes.toString()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val binding =
            FragmentImageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CustomViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return imageResList.size
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.bind(imageResList[position])
    }
}