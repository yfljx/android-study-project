package com.example.study.page.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.study.databinding.FragmentOneKeyClearBinding

class OneKeyClearFragment : Fragment() {
    private lateinit var binding: FragmentOneKeyClearBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentOneKeyClearBinding.inflate(layoutInflater, container, false)
        return binding.root
    }
}