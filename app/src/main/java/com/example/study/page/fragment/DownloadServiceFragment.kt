package com.example.study.page.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.study.databinding.FragmentDownloadServiceBinding
import com.example.study.page.service.DownloadService

class DownloadServiceFragment : Fragment() {
    companion object {
        const val URL =
            "https://www.douyin.com/download/pc/obj/douyin-pc-client/7044145585217083655/releases/10507435/2.4.0/darwin-universal/douyin-v2.4.0-darwin-universal.dmg"
    }

    private lateinit var serviceIntent: Intent

    private lateinit var binding: FragmentDownloadServiceBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentDownloadServiceBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        serviceIntent = Intent(
            this.context, DownloadService::
            class.java
        ).apply {
            putExtra("url", URL) // 将要下载的文件的URL添加到Intent对象中
        }
        binding.download.setOnClickListener {
            activity?.startService(serviceIntent)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        activity?.stopService(serviceIntent)
    }
}