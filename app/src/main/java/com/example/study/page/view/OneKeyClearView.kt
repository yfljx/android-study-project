package com.example.study.page.view

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.SweepGradient
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.animation.DecelerateInterpolator

/**
 * Created by lijinxi on 2024/2/28
 * @Description:
 */
class OneKeyClearView(context: Context, attrs: AttributeSet? = null) :
    View(context, attrs) {
    val paint = Paint()
    private val outCirclePaint = Paint().apply {
        isAntiAlias = true
        color = Color.GRAY
        style = Paint.Style.FILL
    }
    private val textPaint = Paint().apply {
        color = Color.BLACK
        textSize = 30f
        textAlign = Paint.Align.CENTER
    }
    private val outArcPaint = Paint().apply {
        isAntiAlias = true
        strokeWidth = 10f
        style = Paint.Style.STROKE
        strokeCap = Paint.Cap.ROUND
    }
    private val radarPaint = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
    }


    private var outArcWidth: Int = 0

    private var outArcSweepGradient: SweepGradient? = null

    private var targetValue: Float = 50f


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = MeasureSpec.getSize(widthMeasureSpec)
        val height = MeasureSpec.getSize(heightMeasureSpec)
        minOf(width, height).let {
            setMeasuredDimension(it, it)
        }
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val width = measuredWidth
        val height = measuredHeight
        val pointX = width / 2
        val pointY = height / 2

        outArcWidth = (0.9 * width).toInt()

        val rectF = RectF(
            (pointX - outArcWidth / 2).toFloat(),
            (pointY - outArcWidth / 2).toFloat(),
            (pointX + outArcWidth / 2).toFloat(),
            (pointY + outArcWidth / 2).toFloat()
        )

        outArcSweepGradient =
            SweepGradient(pointX.toFloat(), pointY.toFloat(), Color.WHITE, Color.GREEN)
        outArcPaint.shader = outArcSweepGradient
        canvas.drawArc(rectF, 30f, targetValue * 360 / 100 + 30f, false, outArcPaint)

        outCirclePaint.color = Color.GRAY
        canvas.drawCircle(
            pointX.toFloat(),
            pointY.toFloat(),
            (outArcWidth / 2 - 20).toFloat(),
            outCirclePaint
        )

        radarPaint.shader = outArcSweepGradient
        val radarRectF = RectF(
            (pointX - outArcWidth / 2).toFloat() + 20f,
            (pointY - outArcWidth / 2).toFloat() + 20f,
            (pointX + outArcWidth / 2).toFloat() - 20f,
            (pointY + outArcWidth / 2).toFloat() - 20f
        )
        canvas.drawArc(radarRectF, 30f, targetValue * 360 / 100 + 30f, true, radarPaint)

        outCirclePaint.color = Color.WHITE
        canvas.drawCircle(pointX.toFloat(), pointY.toFloat(), 50f, outCirclePaint)

        canvas.drawText(
            "${targetValue.toInt()}%",
            pointX.toFloat(),
            pointY.toFloat() - textPaint.ascent() / 2 - textPaint.descent() / 2,
            textPaint
        )
    }

    private fun change() {
        val originValue = targetValue
        targetValue -= (4 + Math.random() * 20).toFloat()
        if (targetValue < 20) targetValue += 60
        // 动画时长，由外部调用者设定
        val animator = ValueAnimator.ofFloat(originValue, targetValue)
        animator.apply {
            interpolator = DecelerateInterpolator()
            this.duration = 3000
            addUpdateListener {
                targetValue = animatedValue as Float
                invalidate()
            }
            start()
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event?.action == MotionEvent.ACTION_DOWN) {
            change()
            return true
        }
        return super.onTouchEvent(event)
    }
}