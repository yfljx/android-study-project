package com.example.study.page.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.example.study.R
import com.example.study.databinding.FragmentNavBlankBinding


class NavBlankFragment : Fragment() {
    private lateinit var binding: FragmentNavBlankBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =  FragmentNavBlankBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onStart() {
        super.onStart()
        val navController = requireActivity().findNavController(R.id.nav_host_fragment_content_main)
        val currentDestination = navController.currentDestination
        binding.text.text = "${currentDestination?.label} Fragment"
    }
}