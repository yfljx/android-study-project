package com.example.study.page

import androidx.fragment.app.Fragment
import com.example.study.page.fragment.AIDLFragment
import com.example.study.page.fragment.BitmapFragment
import com.example.study.page.fragment.DownloadServiceFragment
import com.example.study.page.fragment.FlowLayoutFragment
import com.example.study.page.fragment.IPCFragment
import com.example.study.page.fragment.JSBridgeFragment
import com.example.study.page.fragment.LifecycleFragment
import com.example.study.page.fragment.LotteryDiskFragment
import com.example.study.page.fragment.MessengerServiceFragment
import com.example.study.page.fragment.NativeFragment
import com.example.study.page.fragment.NavMainFragment
import com.example.study.page.fragment.OkHttpFragment
import com.example.study.page.fragment.OneKeyClearFragment
import com.example.study.page.fragment.PagingFragment
import com.example.study.page.fragment.RadiusCornerLayoutFragment
import com.example.study.page.fragment.TabHostFragment
import com.example.study.page.fragment.TabLayoutFragment
import com.example.study.page.fragment.ViewPagerFragment
import com.example.study.page.fragment.WebViewFragment

/**
 * Created by lijinxi on 2024/2/27
 * @Description:
 */
enum class Pages(private val cls: Class<out Fragment>) {
    FlowLayout(FlowLayoutFragment::class.java),
    RadiusCornerLayout(RadiusCornerLayoutFragment::class.java),
    ViewPager(ViewPagerFragment::class.java),
    TabLayout(TabLayoutFragment::class.java),
    WebView(WebViewFragment::class.java),
    Lifecycle(LifecycleFragment::class.java),
    Navigation(NavMainFragment::class.java),
    Paging(PagingFragment::class.java),
    IPC(IPCFragment::class.java),
    LotteryDisk(LotteryDiskFragment::class.java),
    OneKeyClear(OneKeyClearFragment::class.java),
    DownloadService(DownloadServiceFragment::class.java),
    Native(NativeFragment::class.java),
    OkHttp(OkHttpFragment::class.java),
    Bitmap(BitmapFragment::class.java),
    AIDL(AIDLFragment::class.java),
    JSBridge(JSBridgeFragment::class.java),
    TabHost(TabHostFragment::class.java),
    Messenger(MessengerServiceFragment::class.java);

    fun pageName(): String {
        return when (this) {
            FlowLayout -> "Flow Layout"  // 流式布局
            RadiusCornerLayout -> "Radius Corner Layout"  // 圆角布局
            ViewPager -> "View Pager 2"  // View Pager2 使用
            TabLayout -> "Tab Layout"    // View Pager2 和 TabLayout 联动
            WebView -> "Web View"        // Web View 使用
            Lifecycle -> "Lifecycle"     // 生命周期事件监听
            Navigation -> "Navigation"   // Navigation 使用
            Paging -> "Paging"           // 分页加载
            IPC -> "IPC"                 // 进程间通信，模拟AIDL实现
            LotteryDisk -> "Lottery Disk"    // 抽奖界面
            OneKeyClear -> "One Key Clear"   // 一键清理界面
            DownloadService -> "Download Service"  // 下载服务
            Native -> "Native"       // Native C++ 快速排序
            OkHttp -> "OkHttp"       // 自定义OkHttp - 下载进度
            Bitmap -> "Bitmap"       // Bitmap 使用
            AIDL -> "AIDL"           // AIDL - 冒泡排序
            JSBridge -> "JSBridge"    // JSBridge
            TabHost -> "TabHost"      // TabHost
            Messenger -> "Messenger"   // Messenger 通信
        }
    }

    fun fragment(): Fragment {
        return this.cls.getDeclaredConstructor().newInstance()
    }
}