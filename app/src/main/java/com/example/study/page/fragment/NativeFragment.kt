package com.example.study.page.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.study.R
import com.example.study.databinding.FragmentNativeBinding

class NativeFragment : Fragment() {
    private lateinit var binding: FragmentNativeBinding

    companion object {
        val array = arrayOf(10, 9, 2, 6, 7)
        init {
            System.loadLibrary("study")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentNativeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.text.text = array.joinToString("-")
        binding.button.setOnClickListener {
            binding.text.text = quickSort(array).joinToString("-")
        }
    }

    private external fun quickSort(list: Array<Int>): Array<Int>
}