package com.example.study.page.fragment

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.study.R
import com.example.study.databinding.FragmentFlowLayoutBinding


class FlowLayoutFragment : Fragment() {
    private lateinit var binding: FragmentFlowLayoutBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentFlowLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.flowLayout.apply {
            val tags = listOf(
                "Android", "Kotlin", "Java", "Flutter", "React Native",
                "Swift", "Objective-C", "Xamarin", "Ionic", "Cordova"
            )

            for (tag in tags) {
                Button(context).apply {
                    text = tag
                    setTextColor(ColorStateList.valueOf(Color.BLACK))
                    addView(this)
                }
            }
        }
    }
}