package com.example.study.page.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import com.example.study.R
import com.example.study.databinding.FragmentLifecycleBinding

class LifecycleFragment : Fragment() {
    private lateinit var binding: FragmentLifecycleBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentLifecycleBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.addObserver(LifecycleListener(context))
    }

    inner class LifecycleListener(private val context: Context?) : LifecycleEventObserver {
        override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
            when (event) {
                Lifecycle.Event.ON_CREATE -> {
                    Toast.makeText(context, "ON_CREATE", Toast.LENGTH_SHORT).show()
                }

                else -> {
                    Log.d(this::class.java.simpleName, event.name)
                }
            }
        }

    }

}