package com.example.study

import android.app.Application

/**
 * Created by lijinxi on 2024/3/2
 * @Description:
 */
class CustomApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        SplitManager.create(context = applicationContext)
    }
}