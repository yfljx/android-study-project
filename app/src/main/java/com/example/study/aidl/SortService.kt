package com.example.study.aidl

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.example.study.aidl.SortImpl

/**
 * Created by lijinxi on 2024/3/2
 * @Description:
 */
class SortService : Service() {
    private var sortImpl: SortImpl? = null
    override fun onBind(intent: Intent?): IBinder? {
        return sortImpl
    }

    override fun onCreate() {
        super.onCreate()
        sortImpl = SortImpl()
    }
}