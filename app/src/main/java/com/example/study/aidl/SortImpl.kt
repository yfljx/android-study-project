package com.example.study.aidl

import android.os.RemoteException
import com.example.study.ISort


/**
 * Created by lijinxi on 2024/3/2
 * @Description:
 */
class SortImpl : ISort.Stub() {
    @Throws(RemoteException::class)
    override fun sortArray(array: IntArray) {
        // 使用冒泡排序对数组进行排序
        val n = array.size
        for (i in 0 until n - 1) {
            for (j in 0 until n - i - 1) {
                if (array[j] > array[j + 1]) {
                    // 交换位置
                    val temp = array[j]
                    array[j] = array[j + 1]
                    array[j + 1] = temp
                }
            }
        }
    }
}