package com.example.study.ipc

import android.os.IInterface
import android.os.RemoteException

/**
 * Created by lijinxi on 2024/2/28
 * @Description:
 */
interface IBookManager : IInterface {
    @Throws(RemoteException::class)
    fun getBooks(): List<Book?>?

    @Throws(RemoteException::class)
    fun addBook(book: Book?)
}