package com.example.study.ipc

import android.app.Service
import android.content.Intent
import android.os.IBinder

/**
 * Created by lijinxi on 2024/2/28
 * @Description:
 */
class BookManagerService : Service() {
    private var books = arrayListOf<Book>()

    override fun onBind(intent: Intent?): IBinder? {
        return object : BookManagerStub() {
            override fun getBooks(): List<Book?>? {
                synchronized(books) {
                    if (books.isNotEmpty()) {
                        return books
                    }
                    return arrayListOf()
                }
            }

            override fun addBook(book: Book?) {
                synchronized(books) {
                    if (books.isEmpty()) books = arrayListOf()
                    if (book != null) {
                        books.add(book)
                    }
                }
            }

        }
    }

    override fun onCreate() {
        super.onCreate()
        books.add(Book(20, "三体"))
    }
}