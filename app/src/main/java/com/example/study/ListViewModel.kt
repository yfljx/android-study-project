package com.example.study

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * Created by lijinxi on 2024/2/27
 * @Description:
 */
class ListViewModel : ViewModel() {
    var currentIndex = MutableLiveData(-1)

    var okHttpProgress = MutableLiveData(0)
}