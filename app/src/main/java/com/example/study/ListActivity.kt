package com.example.study

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.InputQueue
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.study.databinding.ActivityListBinding
import com.example.study.page.Pages

class ListActivity : AppCompatActivity() {
    private lateinit var binding: ActivityListBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListBinding.inflate(layoutInflater)

        val viewModel = ViewModelProvider(this)[ListViewModel::class.java]
        viewModel.currentIndex.observe(this@ListActivity) {
            if (it != -1) {
                // 启动新的activity
                val intent = Intent(this@ListActivity, DetailActivity::class.java)
                intent.putExtra("index", it)
                this@ListActivity.startActivity(intent)
            }
        }

        binding.list.apply {
            layoutManager = LinearLayoutManager(this@ListActivity).apply {
                orientation = LinearLayoutManager.VERTICAL
            }
            adapter = CustomRecyclerAdapter(viewModel).apply {
                registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
                    override fun onChanged() {
                        super.onChanged()
                        Log.d(this::class.java.simpleName, "onChanged")
                    }

                    override fun onItemRangeChanged(positionStart: Int, itemCount: Int) {
                        super.onItemRangeChanged(positionStart, itemCount)
                        Log.d(
                            this::class.java.simpleName,
                            "onItemRangeChanged: $positionStart, $itemCount"
                        )
                    }
                })
            }
            addItemDecoration(CustomRecyclerAdapter.CustomItemDecoration(8f, Color.CYAN))
            binding.list.adapter?.notifyItemRangeChanged(0, Pages.entries.size)  //方法一对一
        }

        /*
        window.statusBarColor = Color.TRANSPARENT
        WindowCompat.setDecorFitsSystemWindows(window, true)
        WindowInsetsControllerCompat(window, window.decorView).apply {
            systemBarsBehavior =
                WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            isAppearanceLightStatusBars = true
        }*/
        enableEdgeToEdge()
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(binding.main) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }
}