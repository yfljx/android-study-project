// ISort.aidl
package com.example.study;

// Declare any non-default types here with import statements

interface ISort {
    void sortArray(inout int[] array);
}