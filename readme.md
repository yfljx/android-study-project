# Android - Study Project
本项目源于学习和开发时的Demo，整理后公开，代码基于API 34。

## 代码List
```kotlin
FlowLayout -> "Flow Layout"  // 流式布局
RadiusCornerLayout -> "Radius Corner Layout"  // 圆角布局
ViewPager -> "View Pager 2"  // View Pager2 使用
TabLayout -> "Tab Layout"    // View Pager2 和 TabLayout 联动
WebView -> "Web View"        // Web View 使用
Lifecycle -> "Lifecycle"     // 生命周期事件监听
Navigation -> "Navigation"   // Navigation 使用
Paging -> "Paging"           // 分页加载
IPC -> "IPC"                 // 进程间通信，模拟AIDL实现
LotteryDisk -> "Lottery Disk"    // 抽奖界面
OneKeyClear -> "One Key Clear"   // 一键清理界面
DownloadService -> "Download Service"  // 下载服务
Native -> "Native"       // Native C++ 快速排序
OkHttp -> "OkHttp"       // 自定义OkHttp - 下载进度
Bitmap -> "Bitmap"       // Bitmap 使用
AIDL -> "AIDL"           // AIDL - 冒泡排序
```
## Activity Embedding
https://developer.android.com/guide/topics/large-screens/activity-embedding?hl=zh-cn, 项目对平板和手机有良好适配。

## 截图
![](imgs/1.png)
